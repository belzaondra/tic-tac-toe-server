import { Field, ObjectType } from "type-graphql";
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from "typeorm";

@ObjectType()
@Entity()
export class User extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Field()
  @Column({
    length: 255,
  })
  email!: string;

  @Field()
  @Column("text")
  password!: string;

  @Field()
  @Column()
  isBlocked!: boolean;
}
