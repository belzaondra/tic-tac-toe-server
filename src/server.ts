import cors from "cors";
import "dotenv-safe/config";
import express from "express";
import { buildSchema } from "type-graphql";
import path from "path";
import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./entities/User";
import { UserResolver } from "./resolvers/UserResolver";
import { ApolloServer } from "apollo-server-express";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";
import { TestResolver } from "./resolvers/TestResolver";

const port = process.env.PORT;

const main = async () => {
  const app = express();

  const conn = await createConnection({
    type: "postgres",
    database: "tic-tac-toe",
    username: "postgres",
    password: "admin",
    logging: true,
    synchronize: true,
    migrations: [path.join(__dirname, "./migrations/*")],
    entities: [User],
  });

  conn.runMigrations();

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [TestResolver, UserResolver],
    }),
    context: ({ req, res }) => ({
      req,
      res,
    }),
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({
    app,
    cors: false,
  });

  app.use(
    cors({
      origin: process.env.ORIGIN,
    })
  );

  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
};

main().catch((err) => console.error(err));
